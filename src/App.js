import React, { Component } from "react";
import Header from "./components/Header";
import SectionSimple from "./components/SectionSimple";
import SectionLink from "./components/SectionLink";
import SectionSpeed from "./components/SectionSpeed";
import SectionPartners from "./components/SectionPartners";
import "./styles/style.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <SectionSimple
          classname="section_the-say"
          title="What they said about us"
          description="We are an award winning creative agency based in Wisconsin with a passion for helping our clients grow."
        />

        <SectionSpeed classname="section_speed" />
        <SectionSimple
          classname="section_secret"
          title="The secret of a company is the team"
          description="We are an award winning creative agency based in Wisconsin with a passion for helping our clients grow."
        />
        <SectionPartners classname="section_partners" />
        <SectionSimple
          classname="section_trust"
          title="They trust us because we made great work"
          description="We are an award winning creative agency based in Wisconsin with a passion for helping our clients grow."
        />
        <SectionLink classname="section_link"/>
        <SectionSimple  classname="section_goods"
          title="Some times we share some goods"
          description="We are an award winning creative agency based in Wisconsin with a passion for helping our clients grow." />

        {/*   <Section
          classname="section_speed"
          title="We Change Ideas To Realities"
        />
        <Section
          classname="section_secret"
          title="	The secret of a company is the team"
          description="We are an award winning creative agency based in Wisconsin with a passion for helping our clients grow."
        />
        <Section
          classname="section_partners"
          title="We Change Ideas To Realities"
        />
        <CompanyImages /> */}
      </div>
    );
  }
}

export default App;
