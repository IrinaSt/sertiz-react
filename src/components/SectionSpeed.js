import React, { Component } from 'react';
import TitleTwoColors from './TitleTwoColors'
import DecorationLine from './DecorationLine';

class SectionSpeed extends Component {
  render() {
		const classname = this.props.classname;

    return (
			<section className={classname}>
			<div className="content-size-container">
			<TitleTwoColors children={['we change', "ideas", "to", "realities"]} />
			<DecorationLine classname="orange-line"/>					
			</div>
		</section>
    );
  }
}

export default SectionSpeed;