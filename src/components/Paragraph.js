import React, { Component } from 'react';


class Paragraph extends Component {
  render() {
		let children = this.props.children;
    return (
			<p className="section-paragraph">{children}</p>
    );
  }
}

export default Paragraph;