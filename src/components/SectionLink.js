import React, { Component } from 'react';
import TitleTwoColors from "./TitleTwoColors"
import DecorationLine from "./DecorationLine";

class SectionLink extends Component {
  render() {
		const classname = this.props.classname;

    return (
			<section className={classname}>
			<div className="content-size-container">
			<TitleTwoColors children={['Check what', "said", "our", "bird"]}/>
			<DecorationLine classname="orange-line" />
			<p className="section_link-title"> We have great products at #Themeforest for One Page Website here
				<a href="#" className="text-orange">/item/akwamarin-one-page-pa...</a>
			</p>	
			</div>
			<DecorationLine classname="white-line" />
		</section>
    );
  }
}

export default SectionLink;