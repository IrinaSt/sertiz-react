import React, { Component } from 'react';
import Title from './Title'
import Paragraph from './Paragraph';
import DecorationLine from './DecorationLine';


class SectionSimple extends Component {
  render() {
		const {classname, title, description} = this.props;

    return (
			<section className={classname}>
			<div className="content-size-container">
				<Title children={title} />
				<Paragraph children={description}/>	
				<DecorationLine	classname="orange-line"/>			
			</div>
		</section>
    );
  }
}

export default SectionSimple;