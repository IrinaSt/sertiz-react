import React, { Component } from 'react';


class TitleTwoColors extends Component {
  render() {
		let children = this.props.children;
    return (
			<h3 className="section-title text-white">{children[0]}
				<span className="text-orange">{children[1]}</span>{children[2]}
				<span className="text-orange">{children[3]}</span>
			</h3>
    );
  }
}

export default TitleTwoColors;