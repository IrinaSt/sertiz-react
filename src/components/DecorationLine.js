import React, { Component } from 'react';


class DecorationLine extends Component {
  render() {
    const className = this.props.classname;
    return (
			<hr className={className} />	
    );
  }
}

export default DecorationLine;