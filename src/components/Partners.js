import React, { Component } from 'react';
import dryme from '../img/dryme.png';
import houseHomeLogo from '../img/house-home.png';
import journey from '../img/journy.png';
import nationalPost from '../img/national-post.png';


class Partners extends Component {
  render() {
    return (
			<div className="partners-links">
			<a href="#" className="partner-logo">
			<img src={nationalPost} alt="national-post-logo" /> </a>
			<a href="#" className="partner-logo">
				<img src={dryme} alt="dryme-logo" />
			</a>
			<a href="#" className="partner-logo">
				<img src={houseHomeLogo} alt="house-home-logo" />
			</a>
			<a href="#" className="partner-logo">
				<img src={journey} alt="journy-logo" />
			</a>
		</div>
    );
  }
}

export default Partners;