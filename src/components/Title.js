import React, { Component } from 'react';


class Title extends Component {
  render() {
		let children = this.props.children;
    return (
			<h3 className="section-title">{children}</h3>
    );
  }
}

export default Title;