import React, { Component } from 'react';


class Header extends Component {
  render() {
    return (
			<header className="page-header">
			<h2 className="page-title">Sertiz</h2>
			<p className="header-paragraph">Awesome One Page HTML Templates</p>
			<div className="slogan">
				<div className="slogan_who">we</div>
				<div className="slogan_main-part">make smart design</div>
			</div>
		</header>
    );
  }
}

export default Header;