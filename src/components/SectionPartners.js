import React, { Component } from 'react';
import Partners from "./Partners";
import TitleTwoColors from "./TitleTwoColors"

class SectionPartners extends Component {
  render() {
		const classname = this.props.classname;

    return (
			<section className={classname}>
			<div className="content-size-container">
				<TitleTwoColors children={['we change', "ideas", "to", "realities"]}/>			
				<Partners classname="orange-line" />				
			</div>
		</section>
    );
  }
}

export default SectionPartners;